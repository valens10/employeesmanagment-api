from django.contrib import admin
from employee.models import Position, Employee

admin.site.register(Position)
admin.site.register(Employee)
