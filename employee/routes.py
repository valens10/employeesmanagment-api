from django.urls import path
from employee.views import EmployeeList, EmployeeDetail, PositionList, PositionDetail

urlpatterns = [
    path('employees/GET/', EmployeeList.as_view(), name='employees-list'),
    path('employees/PATCH/GET/PUT/DELETE/<slug:pk>', EmployeeDetail.as_view(), name='employees-detail'),

    path('positions/GET/POST/', PositionList.as_view(), name='positions-list'),
    path('positions/PATCH/GET/DELETE/<slug:pk>', PositionDetail.as_view(), name='positions-detail')
]