# EmployManagementApi

EmployManagementApi is a api project  for dealing with Employ management system
# this file documenting how to set up, run, and test EmployManagementApi apps. 

# url for api documentation using postman      https://documenter.getpostman.com/view/10051255/SWLiYkhF


## Installation

Use the Django and django rest_framework
1. pip install django
2. pip install djangorestframework and django-filters

```bash
to run EmployManagementApi
1. activate virtualenv #source venv/bin/activate
2. run server #python3 manager.py runserver
3. open app in localhost then start to browser EmployManagementApi urls #http://127.0.0.1:8000/
```

## Usage

```EmployManagementApi
And you can test EmployManagementApi in admin panel
# username: 1111111122222222
#password: admin




## Contributing
Pull requests are welcome.

thanks.
