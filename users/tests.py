from django.test import TestCase
from users.models import User
from employee.models import Position


class UserTestCase(TestCase):
    def setUp(self):
        self.position = Position.objects.create(
            title='Manager test'
        )

        self.position.save()
        print('print position',self.position)

    
    def test_create_user(self):
        user = User.objects.create_user(
            name="test user",
            phone_number="+250781920500",
            email="testing@gmail.com",
            national_id="1111111133333333",
            date_of_birth="2000-12-21",
            position=self.position
        )
        user.save()

        # TEST IF USER HAS BEEN CREATED
        self.assertEqual(User.objects.count(), 1)
        user = User.objects.first()

        self.assertNotEqual(user, None)
        self.assertEqual(user.name, 'test user')
        self.assertEqual(user.national_id, '1111111133333333')
        self.assertEqual(user.position.title, 'Manager test')

