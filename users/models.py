from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.contrib.auth.models import User
from users.manager import UserManager
from employee.models import Position
import uuid
import datetime

#custom user/manager registration model
class User(AbstractUser):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=200, editable=True, blank=False, null=False)
    national_id = models.CharField(max_length=20, unique=True)  #national_id should be unique
    phone_number = models.CharField(max_length=15, unique=True) #phone number should be unique
    email = models.EmailField(max_length=200, unique=True, null=False, blank=False) #email. should be unique
    date_of_birth = models.DateField()
    statues = {
        ('ACTIVE', 'ACTIVE'),
        ('INACTIVE', 'INACTIVE'),
    }

    status = models.CharField(max_length=50, choices=statues, default='INACTIVE')
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    registered_time = models.DateTimeField(auto_now_add=True, editable=False)

    USERNAME_FIELD = 'national_id'
    # REQUIRED_FIELDS = ('user',)

    objects = UserManager()

    def save(self,*args, **kwargs):
        if self.national_id:
            self.username = self.national_id
    
        super(User,self).save(*args, **kwargs)
    
    def __str__(self):
        return self.national_id


# def generate_code():
#     key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))    
#      return key

# class Verification(models.Model):
#     code = models.CharField(max_length=8, blank=True)
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     categories = {
#         ('Activation', 'Activation'),
#         ('Reset', 'Reset')
#     }
#     category = models.CharField(max_length=100, choices=categories, default='Activation')
#     is_used = models.BooleanField(default=False)

#     def __str__(self):
#         return self.code

#     def save(self, *args, **kwargs):
#         self.code = generate_code()
#         super(Verification, self).save(*args, **kwargs)

#model that save all employes details in excel file
# class FileUpLoad(models.Model):
#     remark = models.CharField(max_length=200, blank=True, null=True)
#     file = models.FileField(upload_to="media")
#     timestamp = models.DateTimeField(auto_now=True)

#     def __str__(self):
#         return self.title