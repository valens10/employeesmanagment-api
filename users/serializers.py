from users.models import User
from employee.serializers import PositionSerializer
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name', 'national_id', 'phone_number', 'email', 'status', 'date_of_birth', 'registered_time')
    
    def to_representation(self, instance):
        serialized_data = super(UserSerializer, self).to_representation(instance)
        serialized_data['position'] = PositionSerializer(instance.position).data
        return serialized_data