from django.urls import path
from users.views import UserList, UserDetail, manager_register,login,email_confirmation

urlpatterns = [
    path('users/GET/POST/', UserList.as_view(), name='users-list'),
    path('users/PATCH/GET/DELETE/<slug:pk>', UserDetail.as_view(), name='users-detail'),

    path("manager_register/", manager_register, name="manager_register"),
    path("login/", login, name="login"),
    path("activate/<slug:user_id>", email_confirmation)
    # path("upload_excel_file/", upload_employees_list, name="upload_employees_list"),

]